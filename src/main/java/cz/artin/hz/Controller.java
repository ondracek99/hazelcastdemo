package cz.artin.hz;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IExecutorService;
import com.hazelcast.core.ILock;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import cz.artin.hz.entities.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.IntStream;

@RestController
public class Controller {

    @Qualifier("nameMap")
    @Autowired
    IMap<String, String> nameMap;

    @Qualifier("customerMap")
    @Autowired
    IMap<Long, Customer> customerMap;

    @Autowired
    Service service;

    @Autowired
    HazelcastInstance hazelcastInstance;

    Logger LOG = Logger.getLogger(Controller.class.getName());

    @RequestMapping("/putValue")
    public void putValue(@RequestParam(name = "key") String key, @RequestParam(name = "value") String value) {
        nameMap.put(key, value);
    }

    @RequestMapping("/getValue")
    public String getValue(@RequestParam(name = "key") String key) {
        return nameMap.get(key);
    }

    @RequestMapping("/loadData")
    public String loadData() {

        int count = service.findCount();
        int BATCH_SIZE = 10000;
        int totalNumberOfPages = count / BATCH_SIZE;

        Map<Long, Customer> allCustomers = new HashMap<>();

        for (int i = 0; i <= totalNumberOfPages; i++) {
            service.findAll(new PageRequest(i, BATCH_SIZE)).forEach(c -> allCustomers.put(c.getId(), c));
            customerMap.putAll(allCustomers);
            allCustomers.clear();
            LOG.info("Loaded " + i);
        }

        LOG.info("Data loaded in to hazelcast cluster");
        return "Data loaded in to hazelcast cluster";
    }

    @RequestMapping("/getCustomerFromDB")
    public Customer getCustomerFromDB(@RequestParam(name = "id") Long id) {
        return service.getCustomerById(id);
    }

    @RequestMapping("/getCustomerFromCache")
    public Customer getCustomerFromCache(@RequestParam(name = "id") Long id) {
        return customerMap.get(id);
    }

    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public Collection<Customer> sqlPredicate(@RequestBody String query) {
        return customerMap.values(new SqlPredicate(query));
    }

    @RequestMapping("/lockExample")
    public String lockExample() throws InterruptedException {
        ILock myLock = hazelcastInstance.getLock("myLock");
        try {
            LOG.info("Aquiring LOCK");
            myLock.lock();
            LOG.info("Aquired LOCK SUCCESSFULLY");
            Thread.sleep(15000);
            LOG.info("lockExample() executed successfully");
        } finally {
            myLock.unlock();
        }
        return "hello";
    }

    @RequestMapping("/executorExample")
    public String executorExample() throws InterruptedException {
        IExecutorService exec = hazelcastInstance.getExecutorService("exec");
        IntStream.range(1,1000).forEach(i -> {
            exec.submit(new EchoTask(i));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        return "hello";
    }
}
